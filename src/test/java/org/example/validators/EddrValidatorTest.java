package org.example.validators;

import jakarta.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

class EddrValidatorTest {
    private EddrValidator validator;

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        validator = new EddrValidator();
    }

    @Test
    void testValidEddrFormat() {
        assertTrue(validator.isValid("19600214-81648", constraintValidatorContext));
        assertTrue(validator.isValid("1960021481648", constraintValidatorContext));
    }

    @Test
    void testInvalidEddrFormat() {
        assertFalse(validator.isValid("1960021481648X", constraintValidatorContext));
        assertFalse(validator.isValid("19600214-8164", constraintValidatorContext));
        assertFalse(validator.isValid("19600214-816488", constraintValidatorContext));
        assertFalse(validator.isValid("19600214-8164a", constraintValidatorContext));
    }

    @Test
    void testInvalidBirthDate() {
        assertFalse(validator.isValid("20221314-81648", constraintValidatorContext));
        assertFalse(validator.isValid("20200230-81648", constraintValidatorContext));
    }

    @Test
    void testInvalidControlDigit() {
        assertFalse(validator.isValid("19600214-81649", constraintValidatorContext));
    }

    @Test
    void testNullEddrIsValid() {
        assertTrue(validator.isValid(null, constraintValidatorContext));
    }

}