package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.ConstraintViolation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.PrintWriter;
import java.util.Collections;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class WriteToCsvFileTest {
    @Mock
    private PrintWriter mockWriter;

    @Mock
    private ObjectMapper mockMapper;

    @InjectMocks
    private WriteToCsvFile writeToCsvFile;

    @BeforeEach
    public void setUp() {
        writeToCsvFile = new WriteToCsvFile("test.csv");
        MockitoAnnotations.openMocks(this);
        writeToCsvFile.writer = mockWriter;
        writeToCsvFile.mapper = mockMapper;
    }

    @Test
    void testWriteMessageToCSV_SimpleMessage() {
        String message = "Test message";

        writeToCsvFile.writeMessageToCSV(message);

        verify(mockWriter).println(message);
    }

    @Test
    void testWriteMessageToCSV_WithViolations() throws JsonProcessingException {
        String message = "Test message";
        Set<ConstraintViolation<MessagePojo>> violations = Collections.emptySet();
        String jsonErrors = "[]";

        when(mockMapper.writeValueAsString(violations)).thenReturn(jsonErrors);

        writeToCsvFile.writeMessageToCSV(message, violations);

        verify(mockWriter).println(message + " " + jsonErrors);
    }

    @Test
    void testWriteMessageToCSV_WithViolations_JsonProcessingException() throws JsonProcessingException {
        String message = "Test message";
        Set<ConstraintViolation<MessagePojo>> violations = Collections.emptySet();

        when(mockMapper.writeValueAsString(violations)).thenThrow(new JsonProcessingException("Test exception") {});

        writeToCsvFile.writeMessageToCSV(message, violations);

        verify(mockWriter, never()).println(anyString());
    }

    @Test
    void testGetErrorsAsJson() throws JsonProcessingException {
        Set<String> errorMessages = Collections.singleton("Error message");
        Set<ConstraintViolation<MessagePojo>> violations = mockViolations(errorMessages);

        when(mockMapper.writeValueAsString(errorMessages)).thenReturn("[\"Error message\"]");

        String json = writeToCsvFile.getErrorsAsJson(violations);

        assertEquals("[\"Error message\"]", json);
    }

    @Test
    void testClose() {
        writeToCsvFile.close();

        verify(mockWriter).close();
    }

    private Set<ConstraintViolation<MessagePojo>> mockViolations(Set<String> errorMessages) {
        ConstraintViolation<MessagePojo> mockViolation = mock(ConstraintViolation.class);
        when(mockViolation.getMessage()).thenReturn(errorMessages.iterator().next());

        return Collections.singleton(mockViolation);
    }
}