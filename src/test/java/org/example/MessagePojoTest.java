package org.example;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class MessagePojoTest {

    @Test
    void setName() {
        MessagePojo mp = new MessagePojo();
        String name = "Ann";
        mp.setName(name);
        assertEquals(name, mp.getName());
    }

    @Test
    void setCount() {
        MessagePojo mp = new MessagePojo();
        int count = 701;
        mp.setCount(count);
        assertEquals(count, mp.getCount());
    }

    @Test
    void setEddr() {
        MessagePojo mp = new MessagePojo();
        String eddr = "20041026-91553";
        mp.setEddr(eddr);
        assertEquals(eddr, mp.getEddr());
    }

    @Test
    void setCreatedAt() {
        MessagePojo mp = new MessagePojo();
        LocalDateTime createdAt = LocalDateTime.parse("2016-04-27T00:00");
        mp.setCreatedAt(createdAt);
        LocalDateTime actual = mp.getCreatedAt();
        assertEquals(createdAt, actual);
    }
}