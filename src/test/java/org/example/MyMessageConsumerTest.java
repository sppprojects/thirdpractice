package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.jms.*;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class MyMessageConsumerTest {
    @Mock
    private Destination mockDestination;

    @Mock
    private Session session;

    @Mock
    private MessageConsumer consumer;

    @Mock
    private ObjectMapper objectMapper;

    @Mock
    private TextMessage mockTextMessage;

    @Mock
    private Validator mockValidator;

    @Mock
    private WriteToCsvFile mockCsvFileValidWriter;

    @Mock
    private WriteToCsvFile mockCsvFileNotValidWriter;

    @InjectMocks
    private MyMessageConsumer myMessageConsumer;

    @BeforeEach
    public void setup() throws JMSException {
        MockitoAnnotations.openMocks(this);

        when(session.createConsumer(mockDestination)).thenReturn(consumer);

        myMessageConsumer = new MyMessageConsumer(session, mockDestination, mockCsvFileValidWriter, mockCsvFileNotValidWriter);
        myMessageConsumer.setValidator(mockValidator);
        myMessageConsumer.setObjectMapper(objectMapper);
    }

    @Test
    void testHandleMessage_ValidMessage() throws JsonProcessingException {
        MessagePojo expectedMessage = new MessagePojo("Panteleimon", "19600214-81648",
                265, LocalDateTime.parse("2022-05-15T00:00"));
        String jsonMessage = "{\"name\": \"Panteleimon\", \"eddr\": \"19600214-81648\", \"count\": 265, \"created_at\": \"2022-05-15T00:00\"}";

        when(objectMapper.readValue(jsonMessage, MessagePojo.class)).thenReturn(expectedMessage);
        when(mockValidator.validate(any(MessagePojo.class))).thenReturn(Collections.emptySet());

        myMessageConsumer.handleMessage(jsonMessage, objectMapper);

        verify(objectMapper).readValue(jsonMessage, MessagePojo.class);
        verify(mockValidator).validate(expectedMessage);
        verify(mockCsvFileValidWriter).writeMessageToCSV("Panteleimon,265");
        verifyNoInteractions(mockCsvFileNotValidWriter);
    }

    @Test
    void testHandleMessage_PoisonPill() throws JsonProcessingException {
        String jsonMessage = "Time limit is over.";

        myMessageConsumer.handleMessage(jsonMessage, objectMapper);

        verify(objectMapper, never()).readValue(anyString(), any(Class.class));
        verifyNoInteractions(mockValidator, mockCsvFileValidWriter, mockCsvFileNotValidWriter);
    }

    @Test
    void testHandleMessage_MessagesEnded() throws JsonProcessingException {
        String jsonMessage = "Messages ended.";

        myMessageConsumer.handleMessage(jsonMessage, objectMapper);

        verify(objectMapper, never()).readValue(anyString(), any(Class.class));
        verifyNoInteractions(mockValidator, mockCsvFileValidWriter, mockCsvFileNotValidWriter);
    }

    @Test
    void testHandleMessage_InvalidMessage() throws JsonProcessingException {
        String jsonMessage = "{\"name\": \"Philip\", \"eddr\": \"20041026-91553\", \"count\": 829, \"created_at\": \"2016-04-27T00:00\"}";

        MessagePojo invalidMessage = new MessagePojo("Philip", "20041026-91553",
                829, LocalDateTime.parse("2016-04-27T00:00"));
        Set<ConstraintViolation<MessagePojo>> violations = Collections.singleton(mock(ConstraintViolation.class));

        when(objectMapper.readValue(jsonMessage, MessagePojo.class)).thenReturn(invalidMessage);
        when(mockValidator.validate(invalidMessage)).thenReturn(violations);

        myMessageConsumer.handleMessage(jsonMessage, objectMapper);

        verify(objectMapper).readValue(jsonMessage, MessagePojo.class);
        verify(mockValidator).validate(invalidMessage);
        verify(mockCsvFileNotValidWriter).writeMessageToCSV(invalidMessage.toString(), violations);
        verifyNoInteractions(mockCsvFileValidWriter);
    }

    @Test
    void testReadMessages_NonTextMessageHandling() throws JMSException {
        Message nonTextMessage = mock(Message.class);
        when(consumer.receive(1000)).thenReturn(nonTextMessage);

        myMessageConsumer.readMessages();

        verify(consumer, atLeastOnce()).receive(1000);
    }

    @Test
    void testReadMessages_NullMessageHandling() throws JMSException {
        when(consumer.receive(1000)).thenReturn(null);

        myMessageConsumer.readMessages();

        verify(consumer, atLeastOnce()).receive(1000);
    }

    @Test
    void testClose() throws JMSException {
        myMessageConsumer.close();

        verify(consumer).close();
    }

    @Test
    void testReadMessages_Exception() throws JMSException {
        when(consumer.receive(1000)).thenReturn(mockTextMessage);
        doThrow(JMSException.class).when(mockTextMessage).getText();
        myMessageConsumer.readMessages();
        assertThrows(JMSException.class, () -> myMessageConsumer.getTextMessage(mockTextMessage));
    }

    @Test
    void testConstructor_Exception() throws JMSException {
        doThrow(JMSException.class).when(session).createConsumer(any());
        new MyMessageConsumer(session, mockDestination, mockCsvFileValidWriter, mockCsvFileNotValidWriter);
        assertThrows(JMSException.class, () -> session.createConsumer(any()));
    }

    @Test
    void testClose_Exception() throws JMSException {
        doThrow(JMSException.class).when(consumer).close();
        myMessageConsumer.close();
        assertThrows(JMSException.class, () -> consumer.close());
    }
}