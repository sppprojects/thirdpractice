package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.util.Set;

public class MyMessageConsumer {

    private static final Logger consoleLogger = LoggerFactory.getLogger(MyMessageConsumer.class);
    private static final Logger connectionLogger = LoggerFactory.getLogger("ConnectionLogger");

    private MessageConsumer consumer;

    private boolean running = true;

    private final WriteToCsvFile csvFileValidWriter;

    private final WriteToCsvFile csvFileNotValidWriter;

    private ObjectMapper mapper = new ObjectMapper();

    private final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private Validator validator = factory.getValidator();

    public MyMessageConsumer(Session session, Destination consumerDestination,
                             WriteToCsvFile csvFileValidWriter, WriteToCsvFile csvFileNotValidWriter) {
        this.csvFileValidWriter = csvFileValidWriter;
        this.csvFileNotValidWriter = csvFileNotValidWriter;
        try {
            consumer = session.createConsumer(consumerDestination);
        } catch (JMSException e) {
            connectionLogger.error("Can not start the connection", e);
        }
    }

    void setValidator(Validator validator) {
        this.validator = validator;
    }

    void setObjectMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public void readMessages() {
        try {
            mapper.registerModule(new JavaTimeModule());

            while (running) {
                Message message = consumer.receive(5000);

                if (message == null) {
                    running = false;
                    break;
                }

                if (message instanceof TextMessage) {
                    handleMessage(getTextMessage(message), mapper);
                } else {
                    consoleLogger.warn("Received non-text message.");
                    running = false;
                }
            }
        } catch (JMSException | JsonProcessingException e) {
            connectionLogger.error(e.getMessage(), e);
        } finally {
            close();
        }
    }

    public String getTextMessage(Message message) throws JMSException {
        TextMessage textMessage = (TextMessage) message;
        return textMessage.getText();
    }

    public void handleMessage(String text, ObjectMapper mapper) throws JsonProcessingException {
        if (poisonPill(text)) {
            consoleLogger.info("Time limit is over. Shutting down consumer.");
            running = false;
        } else if ("Messages ended.".equals(text)) {
            consoleLogger.info("Shutting down consumer.");
            running = false;
        } else {
            MessagePojo receivedPojo = mapper.readValue(text, MessagePojo.class);
            Set<ConstraintViolation<MessagePojo>> violations = validator.validate(receivedPojo);
            writeToFiles(violations, receivedPojo);
        }
    }

    public void writeToFiles(Set<ConstraintViolation<MessagePojo>> violations, MessagePojo message) {
        if (violations.isEmpty()) {
            csvFileValidWriter.writeMessageToCSV(message.getName() + "," + message.getCount());
        } else {
            csvFileNotValidWriter.writeMessageToCSV(message.toString(), violations);
        }
    }

    private boolean poisonPill(String text) {
        return "Time limit is over.".equals(text);
    }

    public void close() {
        try {
            running = false;
            consumer.close();
            csvFileNotValidWriter.close();
            csvFileValidWriter.close();
            consoleLogger.info("consumer and writers closed.");
        } catch (JMSException e) {
            connectionLogger.error(e.getMessage(), e);
        }
    }
}
