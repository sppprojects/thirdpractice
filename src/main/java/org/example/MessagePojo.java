package org.example;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import org.example.validators.ValidEddr;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;

@JsonPropertyOrder({"name", "eddr", "count", "created_at"})
public class MessagePojo {
    @Length(min = 7, message = "Name should have at least 7 characters")
    @Pattern(regexp = ".*a.*", flags = Pattern.Flag.CASE_INSENSITIVE, message = "Name should have at least one 'a' character")
    @JsonProperty("name")
    private String name;

    @ValidEddr
    @JsonProperty("eddr")
    private String eddr;

    @Min(value = 10, message = "Count should be less than 9")
    @JsonProperty("count")
    private int count;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm")
    @JsonProperty("created_at")
    private LocalDateTime createdAt;

    public MessagePojo() {
    }

    public MessagePojo(String name, String eddr, int count, LocalDateTime createdAt) {
        this.name = name;
        this.eddr = eddr;
        this.count = count;
        this.createdAt = createdAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getEddr() {
        return eddr;
    }

    public void setEddr(String eddr) {
        this.eddr = eddr;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "pojo: {" +
                "name: \"" + name + '\"' +
                ", eddr: \"" + eddr + '\"' +
                ", count: " + String.format("%03d", count) +
                ", created_at: " + createdAt +
                '}';
    }
}
