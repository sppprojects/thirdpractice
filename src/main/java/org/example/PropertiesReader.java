package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Properties;

public class PropertiesReader {
    private static final Logger logger = LoggerFactory.getLogger(PropertiesReader.class);

    public Properties readFile(String fileName) {
        Properties appProps = new Properties();
        try (InputStreamReader reader = new InputStreamReader(Objects.requireNonNull(
                getClass().getClassLoader().getResourceAsStream(fileName)), StandardCharsets.UTF_8)){
            appProps.load(reader);
        } catch (IOException e) {
            logger.error("Can't load file", e);
        }
        return appProps;
    }
}
