package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.ConstraintViolation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Set;
import java.util.stream.Collectors;

public class WriteToCsvFile {

    PrintWriter writer;

    ObjectMapper mapper;

    public WriteToCsvFile(String filePath) {
        try {
            writer = new PrintWriter(new BufferedWriter(new FileWriter(filePath, true)));
            mapper = new ObjectMapper();
        } catch (IOException e) {
            consoleLogger.error("Can not write message to csv.", e);
        }
    }

    private static final Logger consoleLogger = LoggerFactory.getLogger(WriteToCsvFile.class);

    public void writeMessageToCSV(String message) {
            writer.println(message);
    }

    public void writeMessageToCSV(String message, Set<ConstraintViolation<MessagePojo>> violations) {
        try {
            writer.println(message + " " + getErrorsAsJson(violations));
        } catch (JsonProcessingException e) {
            consoleLogger.error("Can not write message to csv.", e);
        }
    }

    public String getErrorsAsJson(Set<ConstraintViolation<MessagePojo>> violations) throws JsonProcessingException {
        Set<String> errorMessages = violations.stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.toSet());
        return mapper.writeValueAsString(errorMessages);
    }

    protected void close() {
        if (writer != null) {
            writer.close();
        }
    }
}
