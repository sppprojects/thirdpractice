package org.example;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class RandomMessagePojoGenerator {
    private final Random random = new Random();

    private static final String[] NAMES = {
            "Olivia", "Emma", "Noah", "Ava", "Oliver", "Sophia", "Elijah",
            "Isabella", "James", "Charlotte", "Amelia", "Lucas", "Mia",
            "Mason", "Ethan", "Diana", "Oleg", "Angelica", "Oleksandr",
            "Angelina", "Ostap", "Paul", "Panteleimon", "Panas", "Peter",
            "Philip", "Rodion", "Semen", "Sergey", "Fedor", "Ian",
            "Oksana", "Yaroslav", "Anastasia", "Veronica", "Margarita"
    };

    private LocalDate dateOfBirthday;

    public MessagePojo generateRandomMessagePojo() {
        String name = generateRandomName();
        String eddr = generateRandomEddr();
        int count = generateRandomCount();
        LocalDateTime createdAt = generateRandomCreatedAt();

        return new MessagePojo(name, eddr, count, createdAt);
    }

    private String generateRandomName() {
        int index = random.nextInt(NAMES.length);
        return NAMES[index];
    }

    private String generateRandomEddr() {
        dateOfBirthday = generateRandomBirthDate();

        int recordNumber = random.nextInt(10000);

        int controlDigit = random.nextInt(10);
        return String.format("%s-%04d%d", dateOfBirthday.format(DateTimeFormatter.ofPattern("yyyyMMdd")),
                recordNumber, controlDigit);
    }

    private int generateRandomCount() {
        return random.nextInt(999) + 1;
    }

    private LocalDate generateRandomBirthDate() {
        int minDay = (int) LocalDate.of(1950, 1, 1).toEpochDay();
        int maxDay = (int) LocalDate.of(2022, 12, 31).toEpochDay();
        long randomDay = minDay + (long)random.nextInt(maxDay - minDay + 1);

        return LocalDate.ofEpochDay(randomDay);
    }

    private LocalDateTime generateRandomCreatedAt() {
        int minDay = (int) dateOfBirthday.toEpochDay();
        int maxDay = (int) LocalDateTime.now().toLocalDate().toEpochDay();
        long randomDay = minDay + (long)random.nextInt(maxDay - minDay + 1);

        return LocalDateTime.ofEpochSecond(randomDay * 24 * 60 * 60, 0, java.time.ZoneOffset.UTC);
    }
}
