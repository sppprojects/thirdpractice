package org.example.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class EddrValidator implements ConstraintValidator<ValidEddr, String> {
    @Override
    public void initialize(ValidEddr constraintAnnotation) { //required
    }

    @Override
    public boolean isValid(String object, ConstraintValidatorContext context) {
        if (object == null) {
            return true;
        }

        if (!object.matches("\\d{8}-\\d{5}") && !object.matches("\\d{13}")) {
            return false;
        }

        return hasValidBirthDate(object) && hasValidControlDigit(object);
    }

    private boolean hasValidBirthDate(String object) {
        String birthDate = object.substring(0, 8);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");

        try {
            LocalDate.parse(birthDate, formatter);
            return true;
        } catch (DateTimeParseException e) {
            return false;
        }
    }

    private boolean hasValidControlDigit(String eddr) {
        eddr = eddr.replace("-", "");

        int[] weights = {7, 3, 1, 7, 3, 1, 7, 3, 1, 7, 3, 1};
        int sum = 0;
        for (int i = 0; i < weights.length; i++) {
            sum += Character.getNumericValue(eddr.charAt(i)) * weights[i];
        }
        int controlDigit = sum % 10;
        return controlDigit == Character.getNumericValue(eddr.charAt(eddr.length() - 1));
    }
}
